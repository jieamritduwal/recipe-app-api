
# # FROM python:3.9-alpine3.14
# # FROM python:3.9.7-alpine3.14
# FROM python:3.10.2-alpine3.14

# LABEL maintainer="Amrit Duwal"

# ENV PYTHONUNBUFFERED 1

# COPY ./requirements.txt /tmp/requirements.txt
# COPY ./requirements.dev.txt /tmp/requirements.dev.txt
# COPY ./scripts /scripts
# # COPY ./scripts/run.sh /run.sh
# COPY ./app /app
# WORKDIR /app
# EXPOSE 8000

# ARG DEV="false"

# RUN python -m venv /py && \
#     source /py/bin/activate && \
#     # apk --update add --no-cache shadow && \
#     /py/bin/pip install Django==5.0.1\
#     /py/bin/pip install --upgrade pip && \
#     apk add --update --no-cache postgresql-client jpeg-dev && \
#     # apk add --update --no-cache postgresql-client=13.3-r0 && \
#     # apk add --update --no-cache postgresql-client=13.12-r0 && \
#     apk add --update --no-cache --virtual .tmp-build-deps \
#         build-base postgresql-dev musl-dev zlib zlib-dev linux-headers && \
#     /py/bin/pip install flake8 && \
#     /py/bin/pip install -r /tmp/requirements.txt && \
#     /py/bin/pip install djangorestframework==3.12.4 && \
#     if [ $DEV = "true" ]; \
#         then /py/bin/pip install -r /tmp/requirements.dev.text ; \
#     fi && \
#     rm -rf /tmp && \
#     apk del .tmp-build-deps && \
#     adduser \
#          --disabled-password \
#          --no-create-home \
#          django-user && \
#     mkdir -p /vol/web/media && \
#     mkdir -p /vol/web/static && \
#     chown -R django-user:django-user /vol && \
#     chmod -R 755 /vol && \
#     chmod -R +x /scripts

# ENV PATH="/scripts:/py/bin:$PATH"

# USER django-user

# CMD ["run.sh"]
# CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]



# # Use the official Python image as the base image
# FROM python:3.10.2-alpine3.14

# LABEL maintainer="Amrit Duwal"

# # Set environment variables
# ENV PYTHONUNBUFFERED 1

# # Copy requirements and application code to the container
# COPY ./requirements.txt /tmp/requirements.txt
# COPY ./requirements.dev.txt /tmp/requirements.dev.txt
# COPY ./scripts /app/scripts
# COPY ./app /app

# # Set the working directory
# WORKDIR /app

# # Install system dependencies
# RUN apk update && \
#     apk add --no-cache postgresql-client jpeg-dev build-base postgresql-dev musl-dev zlib zlib-dev linux-headers && \
#     python -m venv /py && \
#     /py/bin/pip install --upgrade pip

# # Install Python dependencies
# RUN /py/bin/pip install -r /tmp/requirements.txt && \
#     /py/bin/pip install djangorestframework==3.12.4

# # Install development dependencies if DEV is set to "true"
# ARG DEV="false"
# RUN if [ "$DEV" = "true" ]; then /py/bin/pip install -r /tmp/requirements.dev.txt; fi

# # Clean up unnecessary files and dependencies
# RUN rm -rf /tmp && \
#     apk del build-base postgresql-dev musl-dev zlib-dev linux-headers

# RUN python -m venv /venv && \
#     /venv/bin/pip install --upgrade pip && \
#     /venv/bin/pip install -r /tmp/requirements.txt

# RUN apt-get update && apt-get upgrade -y


# # Add a non-root user
# RUN adduser --disabled-password --no-create-home django-user && \
#     /py/bin/pip install dj_database_url==0.5.0 && \
#     /py/bin/pip install -r /tmp/requirements.txt && \
#     /py/bin/pip install djangorestframework==3.12.4 && \
#     mkdir -p /vol/web/media && \
#     mkdir -p /vol/web/static && \
#     chown -R django-user:django-user /vol && \
#     chmod -R 755 /vol && \
#     chmod -R +x /app/scripts

# # Expose port 8000
# EXPOSE 8000

# # Switch to the non-root user
# USER django-user

# # Command to run when the container starts
# # CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
# CMD ["/Users/mac/Documents/Python/api/recipe-app-api/app/venv/bin/bash", "-c", "source /Users/mac/Documents/Python/api/recipe-app-api/app/venv/bin/activate && python manage.py runserver 0.0.0.0:8000"]





# FROM python:3.9-alpine3.14
# FROM python:3.9.7-alpine3.14
FROM python:3.10.2-alpine3.14

LABEL maintainer="Amrit Duwal"

ENV PYTHONUNBUFFERED 1
# RUN echo "" > /tmp/requirements.txt
# RUN echo "" > /tmp/requirements.dev.txt
COPY ./requirements.txt /tmp/requirements.txt
COPY ./requirements.dev.txt /tmp/requirements.dev.txt
COPY ./scripts /scripts
# COPY ./scripts/run.sh /run.sh
COPY ./app /app
WORKDIR /app
EXPOSE 8000

ARG DEV=false

RUN python3 -m venv /py && \
    # apk --update add --no-cache shadow && \
    /py/bin/pip3 install --upgrade pip && \
    apk add --update --no-cache postgresql-client jpeg-dev && \
    # apk add --update --no-cache postgresql-client=13.3-r0 && \
    # apk add --update --no-cache postgresql-client=13.12-r0 && \
    apk add --update --no-cache --virtual .tmp-build-deps \
        build-base postgresql-dev musl-dev zlib zlib-dev linux-headers && \
    /py/bin/pip3 install flake8 && \
    /py/bin/pip3 install -r /tmp/requirements.txt && \
    /py/bin/pip3 install djangorestframework==3.14.0 && \
    if [ $DEV = "true" ]; \
        then /py/bin/pip install -r /tmp/requirements.dev.txt ; \
    fi && \
    rm -rf /tmp && \
    apk del .tmp-build-deps && \
    adduser \
         --disabled-password \
         --no-create-home \
         django-user && \
    mkdir -p /vol/web/media && \
    mkdir -p /vol/web/static && \
    chown -R django-user:django-user /vol && \
    chmod -R 755 /vol && \
    chmod -R +x /scripts

ENV PATH="/scripts:/py/bin:$PATH"

USER django-user

CMD ["run.sh"]